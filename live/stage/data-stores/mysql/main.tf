terraform {
  backend "s3" {}
}

provider "aws" {
  region = "us-east-1"
}

module "mysql" {
  source = "git::ssh://git@gitlab.com/tlindsay42/terraform-up-and-running-modules.git//data-stores/mysql?ref=v0.0.1"

  db_password = "${var.db_password}"
  instance_class = "db.t2.micro"
  allocated_storage = 10
}
