terraform {
  backend "s3" {}
}

provider "aws" {
  region = "us-east-1"
}

module "webserver_cluster" {
  source = "git::ssh://git@gitlab.com/tlindsay42/terraform-up-and-running-modules.git//services/webserver-cluster?ref=v0.0.2"

  cluster_name           = "webservers-stage"
  db_remote_state_bucket = "troy-test-terraform-up-and-running-state"
  db_remote_state_key    = "stage/data-stores/mysql/terraform.tfstate"

  instance_type = "t2.micro"
  min_size      = 2
  max_size      = 2
}

resource "aws_security_group_rule" "allow_testing_inbound" {
  type              = "ingress"
  security_group_id = "${module.webserver_cluster.elb_security_group_id}"

  from_port   = 12345
  to_port     = 12345
  protocol    = "tcp"
  cidr_blocks = ["76.184.119.177/32"]
}
