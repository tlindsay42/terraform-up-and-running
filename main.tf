resource "aws_instance" "example" {
  ami           = "ami-8fd760f6"
  instance_type = "t2.micro"

  tags {
    Name = "terraform-example"
  }
}
